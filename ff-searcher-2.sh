#!/bin/bash

# Check if the required commands are available
if ! command -v rg &> /dev/null; then
    echo "Error: ripgrep (rg) is not installed. Please install it. https://github.com/BurntSushi/ripgrep"
    exit 1
fi

file_with_lines="flags-to-check.txt"
file_with_projects="../projects-to-check.txt"
not_found_file="not_found.txt"
found_file="found.txt"
path_to_search="./downloaded-repos"


# Check if the file with lines exists
if [ ! -f "$file_with_lines" ]; then
   echo "Error: The file '$file_with_lines' does not exist."
   exit 1
fi


rm $not_found_file
rm $found_file
mkdir $path_to_search
cd $path_to_search || exit
# Clone all the projects
current_time=$(date)
echo "Current time: $current_time"
while IFS= read -r git_project; do
    echo "Cloning for: $git_project"
    git clone --depth 1 -q $git_project
done < "$file_with_projects"

#current_time=$(date)

echo "updating all the projects"
ls | xargs -P10 -I{} git -C {} pull --depth 1 -q
#current_time=$(date)

cd ..

while IFS= read -r flag_to_check; do
    echo "Searching for: $flag_to_check"
    result=$(rg -l "$flag_to_check" "$path_to_search" --ignore-file "$file_with_lines")
    if [ -n "$result" ]; then
        echo "$flag_to_check" >> "$found_file"
        echo "$result" >> "$found_file"
    else
        echo "$flag_to_check" >> "$not_found_file"
    fi
done < "$file_with_lines"


echo "Results were added to '$not_found_file' and '$found_file'"
