#!/bin/bash

# Check if the required commands are available
if ! command -v rg &> /dev/null; then
    echo "Error: ripgrep (rg) is not installed. Please install it. https://github.com/BurntSushi/ripgrep"
    exit 1
fi

# Check for the correct number of arguments
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <path_to_search_flags>"
    exit 1
fi
path_to_search="$1"

# Check if the specified path exists
if [ ! -d "$path_to_search" ]; then
    echo "Error: The specified path '$path_to_search' does not exist."
    exit 1
fi

# # Check if the file with lines exists
# if [ ! -f "$file_with_lines" ]; then
#     echo "Error: The file '$file_with_lines' does not exist."
#     exit 1
# fi

file_with_lines="./flags-to-check.txt"
not_found_file="not_found.txt"
found_file="found.txt"

rm $not_found_file
rm $found_file

# Loop through each line in the file and search with ripgrep
while IFS= read -r line; do
    echo "Searching for: $line"
    result=$(rg -l "$line" "$path_to_search" --ignore-file "$file_with_lines")
    if [ -n "$result" ]; then
        echo "$line" >> "$found_file"
        echo "$result" >> "$found_file"
    else
        echo "$line" >> "$not_found_file"
    fi
done < "$file_with_lines"

echo "Results were added to '$not_found_file' and '$found_file'"
