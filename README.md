# feature-flag-checker

This is a simple utility to search for existing feature flags in a folder containing multiples projects to check

## Requirements

1. Install ripgrep : https://github.com/BurntSushi/ripgrep
2. You must have a folder with all the projects you want to check and they must be up to date

## Usage : 

1. Add the list of feature flags to check to `feature-flags.txt`
2. Run the command `./ff-searcher2.sh`
3. You will find the results in two files : 
    1. `found.txt` having the feature flags that were found and where they were found
    2. `not_found.txt` having the feature flags that were not found
4. All the projects will be downloaded `./downloaded-repos` and kept to speed up future usage

# ⚠️ Warning ⚠️
- As this only does a text search you might miss some feature flags if you are concatenating some parts
- Be sure that you have all the projects that might reference a feature flag in the folders you searched to avoid missing a feature flag still existing in some code